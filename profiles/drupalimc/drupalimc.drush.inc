<?php

function drupalimc_drush_command() {
  $items = array();

  $items['generate-feature-links'] = array(
    'description' => "Link features to articles for development",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
  );

  return $items;
}

function drush_drupalimc_generate_feature_links() {
  $feature_results = db_query("SELECT nid from {node} " .
                              "WHERE type='drupalimc_feature'");
  $article_results = db_query("SELECT nid from {node} " .
                              "WHERE type='drupalimc_article'");
  $count = 0;
  while(1) {
    $article_nid = $article_results->fetchField();
    $feature_nid = $feature_results->fetchField();
    if(!($article_nid && $feature_nid)) {
      break;
    }
    drupalimc_features_link_feature($feature_nid, $article_nid);
    $count++;
  }
  print "Linked $count features to articles\n";
}
