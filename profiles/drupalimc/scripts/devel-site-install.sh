#!/bin/sh

drush -y site-install drupalimc
drush en -y views_ui devel devel_generate advanced_help
drush generate-content 100 20 --types=drupalimc_article --skip-fields=field_drupalimc_author
drush generate-content 20 --types=drupalimc_event
drush generate-content 10 --types=drupalimc_feature
drush generate-feature-links
drush cc all
