<?php
/**
 * @file
 * drupalimc_article.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalimc_article_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drupalimc_article_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function drupalimc_article_node_info() {
  $items = array(
    'drupalimc_article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Add a new article to the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
