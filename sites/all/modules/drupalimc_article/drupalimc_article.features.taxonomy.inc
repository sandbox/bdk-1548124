<?php
/**
 * @file
 * drupalimc_article.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drupalimc_article_taxonomy_default_vocabularies() {
  return array(
    'drupalimc_categories' => array(
      'name' => 'IMC Categories',
      'machine_name' => 'drupalimc_categories',
      'description' => 'Categories for Indymedia Articles',
      'hierarchy' => '1',
      'module' => 'drupalimc_article',
      'weight' => '1',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
