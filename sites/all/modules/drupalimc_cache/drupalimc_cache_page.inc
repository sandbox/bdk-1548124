<?php

require_once('drupalimc_cache.const.include');

/**
 * DrupalIMC page cache logic
 *
 * This module tries to cache pages for logged in users in addition anonymous
 * users.  The basic approach is that, for certain pages, we know that they
 * will be the same for all users of the same role.  So we cache those pages 
 * on a per-role basis (where the roles are anonymous, regular user, editor,
 * and administrator).
 */
class DrupalIMCCachePage implements DrupalCacheInterface {
  protected $bin;
  protected $cache_impl;
  protected $cache_role;

  function __construct($bin) {
    $this->bin = $bin;
    $this->cache_impl = new DrupalFileCache($bin);
    $this->cache_role = null;
  }

  function get($cid) {
    $key = $this->cidToKey($cid);
    if($key) {
      return $this->cache_impl->get($key);
    } else {
      return FALSE;
    }
  }

  function getMultiple(&$cids) {
    $keys = array();
    foreach($cids as $cid) {
      $key = $this->cidToKey($cid);
      if($key) {
        $keys[] = $key;
      }
    }
    return $this->cache_impl->getMultiple($keys);
  }

  function set($cid, $data, $expire=CACHE_PERMANENT, array $headers = NULL) {
    $key = $this->cidToKey($cid);
    if($key) {
      $this->cache_impl->set($key, $data, $expire, $headers);
    }
  }

  function clear($cid = NULL, $wildcard = FALSE) {
    if(isset($cid)) {
      // Need to clear the page cache for all roles
      foreach(array('anon', 'user', 'editor', 'admin') as $role) {
        $this->cache_impl->clear($role . ':' . $cid, $wildcard);
      }
    } else {
      // for $cid=null, we can just pass it call through

      // FIXME: Calling DrupalFileCache->clear() doesn't work,  It may be 
      // because the user variable is not set up yet, or maybe the code is 
      // just messed up.  This hack fixes things.
      $this->cache_impl->clear('*', TRUE);
    }
  }

  function isEmpty() {
    return $this->cache_impl->isEmpty();
  }

  // Generate key to use for a cid value.  We will prepend the cid with a
  // prefix based on how we can cache the page.  For example pages that we 
  // have to cache per-role are prepended with "<cache-role>:".  Pages that 
  // can be cached for all users are preperend with "anon:"
  function cidToKey($cid) {
    $request_path = request_path();
    if($this->cachePageForAllUsers($request_path)) {
      return 'anon:' . $cid;
    } else if($this->cachePagePerRole($request_path)) {
      $cache_role = $this->getCacheRole();
      return $cache_role . ':' . $cid;
    } else {
      drupal_page_is_cacheable(FALSE);
      return null;
    }
  }

  function getCacheRole() {
    if(isset($this->cache_role)) {
      return $this->cache_role;
    }
    if(!isset($_COOKIE[DRUPALIMC_CACHE_COOKIE_NAME])) {
      $this->cache_role = 'anon';
    } else {
      $this->cache_role = $_COOKIE[DRUPALIMC_CACHE_COOKIE_NAME];
    }

    return $this->cache_role;
  }

  // Can we serve a cached page to all users?
  function cachePageForAllUsers($request_path) {
    if(!isset($_COOKIE[session_name()])) {
      # No session info set.  We should be able to cache the page
      return TRUE;
    }
    return FALSE;
  }

  // Can we serve a cached page to all users with the same role?
  function cachePagePerRole($request_path) {
    if(in_array($request_path, array('', 'local', 'calendar', 'features')) ||
      preg_match('/node\/\d+$/', $request_path)) {
      return TRUE;
    }
    return FALSE;
  }
}
