<?php
/**
 * @file
 * drupalimc_calendar.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function drupalimc_calendar_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'drupalimc_calendar';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Calendar';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Calendar';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'fullcalendar';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Date/Time */
  $handler->display->display_options['fields']['field_drupalimc_date']['id'] = 'field_drupalimc_date';
  $handler->display->display_options['fields']['field_drupalimc_date']['table'] = 'field_data_field_drupalimc_date';
  $handler->display->display_options['fields']['field_drupalimc_date']['field'] = 'field_drupalimc_date';
  $handler->display->display_options['fields']['field_drupalimc_date']['label'] = '';
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_drupalimc_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['field_drupalimc_date']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'drupalimc_event' => 'drupalimc_event',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'calendar';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Calendar';
  $handler->display->display_options['menu']['weight'] = '10';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['drupalimc_calendar'] = $view;

  $view = new view;
  $view->name = 'drupalimc_upcoming_events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Upcoming Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Upcoming Events';
  $handler->display->display_options['css_class'] = 'clearfix';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '8';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Footer: Global: Link */
  $handler->display->display_options['footer']['link']['id'] = 'link';
  $handler->display->display_options['footer']['link']['table'] = 'views';
  $handler->display->display_options['footer']['link']['field'] = 'link';
  $handler->display->display_options['footer']['link']['label'] = 'add event';
  $handler->display->display_options['footer']['link']['empty'] = TRUE;
  $handler->display->display_options['footer']['link']['text'] = 'Add Event';
  $handler->display->display_options['footer']['link']['path'] = 'add-event';
  $handler->display->display_options['footer']['link']['class'] = 'add-event';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No upcoming calendar events.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Field: Content: Date/Time */
  $handler->display->display_options['fields']['field_drupalimc_date']['id'] = 'field_drupalimc_date';
  $handler->display->display_options['fields']['field_drupalimc_date']['table'] = 'field_data_field_drupalimc_date';
  $handler->display->display_options['fields']['field_drupalimc_date']['field'] = 'field_drupalimc_date';
  $handler->display->display_options['fields']['field_drupalimc_date']['label'] = '';
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_drupalimc_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_date']['settings'] = array(
    'format_type' => 'drupalimc_calendar_short',
    'fromto' => 'value',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_drupalimc_date']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Sort criterion: Content: Date/Time -  start date (field_drupalimc_date) */
  $handler->display->display_options['sorts']['field_drupalimc_date_value']['id'] = 'field_drupalimc_date_value';
  $handler->display->display_options['sorts']['field_drupalimc_date_value']['table'] = 'field_data_field_drupalimc_date';
  $handler->display->display_options['sorts']['field_drupalimc_date_value']['field'] = 'field_drupalimc_date_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'drupalimc_event' => 'drupalimc_event',
  );
  /* Filter criterion: Content: Date/Time -  start date (field_drupalimc_date) */
  $handler->display->display_options['filters']['field_drupalimc_date_value']['id'] = 'field_drupalimc_date_value';
  $handler->display->display_options['filters']['field_drupalimc_date_value']['table'] = 'field_data_field_drupalimc_date';
  $handler->display->display_options['filters']['field_drupalimc_date_value']['field'] = 'field_drupalimc_date_value';
  $handler->display->display_options['filters']['field_drupalimc_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_drupalimc_date_value']['default_date'] = '-2 hours';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['drupalimc_upcoming_events'] = $view;

  return $export;
}
