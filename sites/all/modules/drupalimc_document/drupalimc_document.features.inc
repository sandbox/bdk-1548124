<?php
/**
 * @file
 * drupalimc_document.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalimc_document_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function drupalimc_document_node_info() {
  $items = array(
    'drupalimc_document' => array(
      'name' => t('Document'),
      'base' => 'node_content',
      'description' => t('Document for the website.  Examples: contact us page, links page, principals of unity.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
