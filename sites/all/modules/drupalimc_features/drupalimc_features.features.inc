<?php
/**
 * @file
 * drupalimc_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalimc_features_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "relation" && $api == "relation_type_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drupalimc_features_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function drupalimc_features_image_default_styles() {
  $styles = array();

  // Exported image style: drupalimc_feature_image
  $styles['drupalimc_feature_image'] = array(
    'name' => 'drupalimc_feature_image',
    'effects' => array(
      9 => array(
        'label' => 'Autorotate',
        'help' => 'Add autorotate image based on EXIF Orientation.',
        'effect callback' => 'imagecache_autorotate_image',
        'module' => 'imagecache_autorotate',
        'name' => 'imagecache_autorotate',
        'data' => array(),
        'weight' => '-10',
      ),
      7 => array(
        'label' => 'Javascript crop',
        'help' => 'Create a crop with a javascript toolbox.',
        'effect callback' => 'imagecrop_effect',
        'form callback' => 'imagecrop_effect_form',
        'summary theme' => 'imagecrop_effect_summary',
        'module' => 'imagecrop',
        'name' => 'imagecrop_javascript',
        'data' => array(
          'width' => '680',
          'height' => '300',
          'xoffset' => '',
          'yoffset' => '',
          'resizable' => 1,
          'downscaling' => 0,
          'aspect_ratio' => 'KEEP',
          'disable_if_no_data' => 0,
        ),
        'weight' => '-9',
      ),
      8 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '680',
          'height' => '300',
        ),
        'weight' => '-8',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function drupalimc_features_node_info() {
  $items = array(
    'drupalimc_feature' => array(
      'name' => t('Feature'),
      'base' => 'node_content',
      'description' => t('Featured Article to show at the top of the homepage'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'drupalimc_migrated_feature' => array(
      'name' => t('Migrated Feature'),
      'base' => 'node_content',
      'description' => t('Feature article migrated from an old site'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
