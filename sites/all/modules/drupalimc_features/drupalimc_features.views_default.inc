<?php
/**
 * @file
 * drupalimc_features.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function drupalimc_features_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'drupalimc_feature_feed';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Features';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  $handler->display->display_options['row_options']['links'] = 1;
  $handler->display->display_options['row_options']['comments'] = 0;
  /* Relationship: Content: Relation: is a feature for (node -> relation) */
  $handler->display->display_options['relationships']['relation_base_left_drupalimc_feature']['id'] = 'relation_base_left_drupalimc_feature';
  $handler->display->display_options['relationships']['relation_base_left_drupalimc_feature']['table'] = 'node';
  $handler->display->display_options['relationships']['relation_base_left_drupalimc_feature']['field'] = 'relation_base_left_drupalimc_feature';
  $handler->display->display_options['relationships']['relation_base_left_drupalimc_feature']['required'] = 0;
  $handler->display->display_options['relationships']['relation_base_left_drupalimc_feature']['r_index'] = '1';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['filter_groups']['operator'] = 'OR';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'AND',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'node';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['value'] = '1';
  $handler->display->display_options['filters']['status_1']['group'] = 2;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type_1']['id'] = 'type_1';
  $handler->display->display_options['filters']['type_1']['table'] = 'node';
  $handler->display->display_options['filters']['type_1']['field'] = 'type';
  $handler->display->display_options['filters']['type_1']['value'] = array(
    'drupalimc_migrated_feature' => 'drupalimc_migrated_feature',
  );
  $handler->display->display_options['filters']['type_1']['group'] = 2;
  /* Filter criterion: Relation: Rid */
  $handler->display->display_options['filters']['rid']['id'] = 'rid';
  $handler->display->display_options['filters']['rid']['table'] = 'relation';
  $handler->display->display_options['filters']['rid']['field'] = 'rid';
  $handler->display->display_options['filters']['rid']['relationship'] = 'relation_base_left_drupalimc_feature';
  $handler->display->display_options['filters']['rid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['rid']['group'] = 1;

  /* Display: Feed */
  $handler = $view->new_display('feed', 'Feed', 'feed_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['row_plugin'] = 'node_rss';
  $handler->display->display_options['row_options']['relationship'] = 'field_drupalimc_node_nid';
  $handler->display->display_options['row_options']['links'] = 1;
  $handler->display->display_options['path'] = 'features.xml';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'features';
  $export['drupalimc_feature_feed'] = $view;

  $view = new view;
  $view->name = 'drupalimc_feature_slider';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feature Slider';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['class'] = 'slides';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_drupalimc_node' => 'field_drupalimc_node',
    'field_drupalimc_feature_image' => 'field_drupalimc_feature_image',
  );
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 0;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['empty'] = FALSE;
  $handler->display->display_options['footer']['area']['content'] = '<a href="features">View All Features</a>';
  $handler->display->display_options['footer']['area']['format'] = 'filtered_html';
  $handler->display->display_options['footer']['area']['tokenize'] = 0;
  /* Relationship: Content: Relation: is a feature for (node <-> node) */
  $handler->display->display_options['relationships']['relation_drupalimc_feature_node']['id'] = 'relation_drupalimc_feature_node';
  $handler->display->display_options['relationships']['relation_drupalimc_feature_node']['table'] = 'node';
  $handler->display->display_options['relationships']['relation_drupalimc_feature_node']['field'] = 'relation_drupalimc_feature_node';
  $handler->display->display_options['relationships']['relation_drupalimc_feature_node']['required'] = 1;
  $handler->display->display_options['relationships']['relation_drupalimc_feature_node']['r_index'] = '0';
  /* Field: Featured nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'relation_drupalimc_feature_node';
  $handler->display->display_options['fields']['nid']['ui_name'] = 'Featured nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nid']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['nid']['link_to_node'] = 0;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['id'] = 'field_drupalimc_feature_image';
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['table'] = 'field_data_field_drupalimc_feature_image';
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['field'] = 'field_drupalimc_feature_image';
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['label'] = '';
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['settings'] = array(
    'image_style' => 'drupalimc_feature_image',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_drupalimc_feature_image']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'node/[field_drupalimc_node_1]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_class'] = 'drupalimc-feature-title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 0;
  /* Field: Content: Tagline */
  $handler->display->display_options['fields']['field_drupalimc_tagline']['id'] = 'field_drupalimc_tagline';
  $handler->display->display_options['fields']['field_drupalimc_tagline']['table'] = 'field_data_field_drupalimc_tagline';
  $handler->display->display_options['fields']['field_drupalimc_tagline']['field'] = 'field_drupalimc_tagline';
  $handler->display->display_options['fields']['field_drupalimc_tagline']['label'] = '';
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['text'] = '<h2><a href="node/[nid]">[title]</a></h2>
<p class="drupalimc-feature-tagline"><a href="node/[nid]">[field_drupalimc_tagline]</a></p>';
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_drupalimc_tagline']['element_class'] = 'drupalimc-feature-text';
  $handler->display->display_options['fields']['field_drupalimc_tagline']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_drupalimc_tagline']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'drupalimc_feature' => 'drupalimc_feature',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['drupalimc_feature_slider'] = $view;

  return $export;
}
