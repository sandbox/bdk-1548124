(function ($) {
  // Store our function as a property of Drupal.behaviors.
  Drupal.behaviors.drupalimcFeatures = {
    attach: function (context, settings) {
      if($.flexslider) {
        $('.view-drupalimc-feature-slider .item-list', context).flexslider({
          'animation': 'slide',
          'prevText': '',
          'nextText': '',
          'controlNav': false,
        });
        }
    }
  }
}(jQuery));
