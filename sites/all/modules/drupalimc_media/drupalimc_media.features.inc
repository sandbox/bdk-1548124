<?php
/**
 * @file
 * drupalimc_media.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalimc_media_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function drupalimc_media_image_default_styles() {
  $styles = array();

  // Exported image style: drupalimc_force_large
  $styles['drupalimc_force_large'] = array(
    'name' => 'drupalimc_force_large',
    'effects' => array(
      6 => array(
        'label' => 'Autorotate',
        'help' => 'Add autorotate image based on EXIF Orientation.',
        'effect callback' => 'imagecache_autorotate_image',
        'module' => 'imagecache_autorotate',
        'name' => 'imagecache_autorotate',
        'data' => array(),
        'weight' => '-10',
      ),
      5 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '620',
          'height' => '480',
        ),
        'weight' => '-9',
      ),
    ),
  );

  // Exported image style: drupalimc_large
  $styles['drupalimc_large'] = array(
    'name' => 'drupalimc_large',
    'effects' => array(
      2 => array(
        'label' => 'Autorotate',
        'help' => 'Add autorotate image based on EXIF Orientation.',
        'effect callback' => 'imagecache_autorotate_image',
        'module' => 'imagecache_autorotate',
        'name' => 'imagecache_autorotate',
        'data' => array(),
        'weight' => '-10',
      ),
      1 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '620',
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => '-9',
      ),
    ),
  );

  // Exported image style: drupalimc_small
  $styles['drupalimc_small'] = array(
    'name' => 'drupalimc_small',
    'effects' => array(
      4 => array(
        'label' => 'Autorotate',
        'help' => 'Add autorotate image based on EXIF Orientation.',
        'effect callback' => 'imagecache_autorotate_image',
        'module' => 'imagecache_autorotate',
        'name' => 'imagecache_autorotate',
        'data' => array(),
        'weight' => '-10',
      ),
      3 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '250',
          'height' => '250',
          'upscale' => 0,
        ),
        'weight' => '-9',
      ),
    ),
  );

  // Exported image style: drupalimc_thumbnail
  $styles['drupalimc_thumbnail'] = array(
    'name' => 'drupalimc_thumbnail',
    'effects' => array(
      12 => array(
        'label' => 'Autorotate',
        'help' => 'Add autorotate image based on EXIF Orientation.',
        'effect callback' => 'imagecache_autorotate_image',
        'module' => 'imagecache_autorotate',
        'name' => 'imagecache_autorotate',
        'data' => array(),
        'weight' => '1',
      ),
      13 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '100',
          'height' => '100',
          'upscale' => 1,
        ),
        'weight' => '2',
      ),
    ),
  );

  return $styles;
}
