<?php

/**
 * Implements hook_field_widget_info().
 */
function drupalimc_media_field_widget_info() {
  return array(
    'drupalimc_media_file_plupload' => array(
      'label' => t('Multiple File Upload'),
      'field types' => array('image'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
      'settings' => array('display_field' => FALSE),
    ),
  );
}


 /**
 * Implements hook_field_widget_form().
 *
 */
function drupalimc_media_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  form_load_include($form_state, 'inc', 'file', 'file.field');
  form_load_include($form_state, 'inc', 'image', 'image.field');

  if($items) {
    // Get form elements for the current items.
    // Don't use file_widget_multiple.  It's too complicated for our purposes.
    $current_files = array(
      '#theme' => 'drupalimc_media_existing_pluploads',
    );
    foreach($items as $delta => $item) {
      $file_element = array(
        '#uri' => $item['uri'],
        '#filename' => $item['filename']
      );
      $file_element['fid'] = array(
        '#type' => 'hidden',
        '#value' => $item['fid'],
      );
      $file_element['weight'] = array(
        '#type' => 'textfield',
        '#size' => 2,
        '#default_value' => $delta,
        '#attributes' => array('class' => array('drupalimc-media-gallery-weight')),
      );
      $file_element['delete'] = array(
        '#type' => 'checkbox',
      );
      $current_files[] = $file_element;
    }
  }

  // Create plupload element for new uploads
  $plupload = array(
    '#type' => 'plupload',
    '#upload_validators' => file_field_widget_upload_validators($field, $instance),
    '#extended' => TRUE,
    '#element_validate' => array('file_managed_file_validate', 'drupalimc_media_plupload_validate'),
    '#upload_location' => file_field_widget_uri($field, $instance),
  );
  // We only need a title if we have existing uploads.  Otherwise it's not 
  // really needed.
  if($items) {
    $plupload['#title'] = t('Upload more files');
  }

  // Combine the 2 in a fieldset for the full widget
  $widget_form = array(
    '#type' => 'fieldset',
    '#title' => $element['#title'],
  );
  if($items) {
    $widget_form['current_files'] = $current_files;
  }
  $widget_form['new_files'] = $plupload;
  return $widget_form;
}

function drupalimc_media_plupload_validate($element, &$form_state) {
  $all_files = array();

  // Get existing uploads
  if(isset($form_state['values']['field_drupalimc_gallery'][LANGUAGE_NONE]['current_files'])) {
    $current_files = $form_state['values']['field_drupalimc_gallery'][LANGUAGE_NONE]['current_files'];
    foreach($current_files as $current_file) {
      if(!$current_file['delete']) {
        $all_files[] = array(
          'fid' => $current_file['fid'],
          '#weight' => $current_file['weight'],
        );
      }
    }
    uasort($all_files, 'element_sort');
  }

  // Add new uploads
  $all_files = array_merge($all_files, drupalimc_media_handle_pluploads($element, $form_state));

  // Replace our value
  $form_state['values']['field_drupalimc_gallery'][LANGUAGE_NONE] = $all_files;
}

function drupalimc_media_handle_pluploads($element, &$form_state) {
  $retval = array();

  $default_scheme = variable_get('file_default_scheme', 'public') . '://';
  $dest_dir = isset($element['#upload_location']) ? $element['#upload_location'] : NULL;
  if($dest_dir) {
    file_prepare_directory($dest_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
  } else {
  }

  $uploads = $form_state['values']['field_drupalimc_gallery'][LANGUAGE_NONE]['new_files'];
  foreach($uploads as $uploaded_file) {
    if ($uploaded_file['status'] == 'done') {
      $source = $uploaded_file['tmppath'];
      if($dest_dir) {
        $destination = file_stream_wrapper_uri_normalize($dest_dir . '/' . $uploaded_file['name']);
      } else {
        $destination = file_stream_wrapper_uri_normalize($scheme . $uploaded_file['name']);
      }
      // Rename it to its original name, and put it in its final home.
      // Note - not using file_move here because if we call file_get_mime
      // (in file_uri_to_object) while it has a .tmp extension, it horks.
      $destination = file_unmanaged_move($source, $destination, FILE_EXISTS_RENAME);
      $file = file_uri_to_object($destination);
      file_save($file);
      $retval[] = array('fid' => $file->fid);
    }
  }
  return $retval;
}

function theme_drupalimc_media_existing_pluploads($variables) {
  $element = $variables['element'];

  $header = array(
    array('data' => t('Uploaded file')),
    array('data' => t('Filename')),
    array('data' => t('Weight')),
    array(
      'data' => t('Delete'),
      'style' => 'text-align: center;',
    ),
  );

  $rows = array();
  foreach (element_children($element) as $delta) {
    $row = array();

    $row[] = theme('image_style', array(
        'style_name' => 'thumbnail',
        'path' => $element[$delta]['#uri'],
    ));

    $filename = $element[$delta]['#filename'];
    if(strlen($filename) > 30) {
      $filename = substr($filename, 0, 27) . '...';
    }
    $row[] = $filename;
    $row[] = drupal_render($element[$delta]['weight']);
    $row[] = array(
      'data' => drupal_render($element[$delta]) . drupal_render_children($element[$delta]),
      'align' => 'center',
    );
    $rows[] = array(
      'data' => $row,
      'class' => array('draggable'),
    );
  }

  $table_id = 'drupalimc-media-gallery-existing';
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'drupalimc-media-gallery-weight');

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => $table_id,
      'width' => '100%',
    ),
  ));
}

function drupalimc_media_field_formatter_info() {
  return array(
    'drupalimc_media_text_summary_or_trimmed' => array(
      'label' => t('Text w/ Media Summary or trimmed'),
      'field types' => array('text_with_summary'),
      'settings' => array('trim_length' => 1000),
    ),
    'drupalimc_media_gallery' => array(
      'label' => t('Image Gallery view'),
      'field types' => array('image'),
      'settings' => array(),
    ),
  );
}

function drupalimc_media_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();

  switch($display['type']) {

  case 'drupalimc_media_text_summary_or_trimmed':

    foreach ($items as $delta => $item) {
      if (!empty($item['summary'])) {
        $output = _text_sanitize($instance, $langcode, $item, 'summary');
      }
      else {
        $trimmed_value = text_summary($item['value'], NULL, 600);
        if($instance['settings']['text_processing']) {
          $output = check_markup($trimmed_value, $item['format'], $langcode);
        } else {
          check_plain($trimmed_value);
        }
      }
      $element[$delta] = array('#markup' => $output);
    }
  break;

  case 'drupalimc_media_gallery':
    foreach ($items as $delta => $item) {
      $img = theme('image_style', array(
        'style_name' => 'drupalimc_thumbnail',
        'path' => $item['uri'],
      ));

      $href = image_style_url('drupalimc_large', $item['uri']);

      $element[$delta] = array(
        '#markup' => l($img, $href, array(
        'attributes' => array(
          'class' => 'drupalimc-media-gallery',
        ),
        'html' => TRUE,
      )));
    }
    break;

  default:
    foreach ($items as $delta => $item) {
      $element['$delta'] = array('#markup' => 'no code yet');
    }
  }

  return $element;
}

