<?php
/**
 * @file
 * drupalimc_media.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function drupalimc_media_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__drupalimc_large__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '270',
    'height' => '270',
    'wysiwyg_only' => 0,
    'link_to_file' => 1,
  );
  $export['application__drupalimc_large__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__drupalimc_small_left__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '180',
    'height' => '180',
    'wysiwyg_only' => 0,
  );
  $export['application__drupalimc_small_left__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'application__drupalimc_small_right__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '180',
    'height' => '180',
    'wysiwyg_only' => 0,
  );
  $export['application__drupalimc_small_right__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__drupalimc_large__drupalimc_wysiwyg_placeholder';
  $file_display->weight = -46;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '620',
    'height' => '30',
  );
  $export['audio__drupalimc_large__drupalimc_wysiwyg_placeholder'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__drupalimc_large__file_field_mediaelement_audio';
  $file_display->weight = -45;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'controls' => 1,
    'width' => '620',
    'height' => '30',
    'download_link' => 1,
    'download_text' => 'Download',
  );
  $export['audio__drupalimc_large__file_field_mediaelement_audio'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__drupalimc_small_left__drupalimc_wysiwyg_placeholder';
  $file_display->weight = -46;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '250',
    'height' => '30',
  );
  $export['audio__drupalimc_small_left__drupalimc_wysiwyg_placeholder'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__drupalimc_small_left__file_field_mediaelement_audio';
  $file_display->weight = -45;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'controls' => 1,
    'width' => '250',
    'height' => '30',
    'download_link' => 1,
    'download_text' => 'Download',
  );
  $export['audio__drupalimc_small_left__file_field_mediaelement_audio'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__drupalimc_small_right__drupalimc_wysiwyg_placeholder';
  $file_display->weight = -46;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '250',
    'height' => '30',
  );
  $export['audio__drupalimc_small_right__drupalimc_wysiwyg_placeholder'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__drupalimc_small_right__file_field_mediaelement_audio';
  $file_display->weight = -45;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'controls' => 1,
    'width' => '250',
    'height' => '30',
    'download_link' => 1,
    'download_text' => 'Download',
  );
  $export['audio__drupalimc_small_right__file_field_mediaelement_audio'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__drupalimc_large__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '270',
    'height' => '270',
    'link_to_file' => 1,
  );
  $export['default__drupalimc_large__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__drupalimc_small_left__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '180',
    'height' => '180',
    'link_to_file' => 1,
  );
  $export['default__drupalimc_small_left__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'default__drupalimc_small_right__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '180',
    'height' => '180',
    'link_to_file' => 1,
  );
  $export['default__drupalimc_small_right__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__drupalimc_large__drupalimc_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'drupalimc_large',
    'float' => '',
    'link_to_original' => 1,
  );
  $export['image__drupalimc_large__drupalimc_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__drupalimc_small_left__drupalimc_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'drupalimc_small',
    'float' => 'left',
    'link_to_original' => 1,
  );
  $export['image__drupalimc_small_left__drupalimc_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__drupalimc_small_right__drupalimc_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'drupalimc_small',
    'float' => 'right',
    'link_to_original' => 1,
  );
  $export['image__drupalimc_small_right__drupalimc_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__drupalimc_large__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '270',
    'height' => '270',
    'wysiwyg_only' => 0,
    'link_to_file' => 1,
  );
  $export['text__drupalimc_large__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__drupalimc_small_left__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '180',
    'height' => '180',
    'wysiwyg_only' => 0,
  );
  $export['text__drupalimc_small_left__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'text__drupalimc_small_right__drupalimc_file_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '180',
    'height' => '180',
    'wysiwyg_only' => 0,
  );
  $export['text__drupalimc_small_right__drupalimc_file_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__drupalimc_large__drupalimc_wysiwyg_placeholder';
  $file_display->weight = -40;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '620',
    'height' => '480',
  );
  $export['video__drupalimc_large__drupalimc_wysiwyg_placeholder'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__drupalimc_large__file_field_mediaelement_video';
  $file_display->weight = -39;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'controls' => 1,
    'width' => '620',
    'height' => '480',
    'download_link' => 1,
    'download_text' => 'Download',
  );
  $export['video__drupalimc_large__file_field_mediaelement_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__drupalimc_large__media_bliptv_image';
  $file_display->weight = -47;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'drupalimc_large',
  );
  $export['video__drupalimc_large__media_bliptv_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__drupalimc_large__media_bliptv_video';
  $file_display->weight = -48;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '620',
    'height' => '480',
    'autoplay' => 0,
  );
  $export['video__drupalimc_large__media_bliptv_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__drupalimc_large__media_youtube_image';
  $file_display->weight = -49;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'drupalimc_force_large',
  );
  $export['video__drupalimc_large__media_youtube_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__drupalimc_large__media_youtube_video';
  $file_display->weight = -50;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '620',
    'height' => '480',
    'autoplay' => 0,
  );
  $export['video__drupalimc_large__media_youtube_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__media_preview__media_bliptv_video';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'width' => '560',
    'height' => '340',
    'autoplay' => 0,
  );
  $export['video__media_preview__media_bliptv_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__media_preview__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'width' => '560',
    'height' => '340',
    'autoplay' => 0,
  );
  $export['video__media_preview__media_youtube_video'] = $file_display;

  return $export;
}
