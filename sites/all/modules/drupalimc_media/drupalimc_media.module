<?php

include_once('drupalimc_media.features.inc');
include_once('drupalimc_media.fields.inc');

/*
 * Implements hook_entity_info_alter
 */
function drupalimc_media_entity_info_alter(&$entity_info) {
  // Add extra view modes for file
  $entity_info['file']['view modes']['drupalimc_large'] = array(
    'label' => t('Large / Centered'),
    'custom settings' => TRUE,
  );

  $entity_info['file']['view modes']['drupalimc_small_left'] = array(
    'label' => t('Small / Floating left'),
    'custom settings' => TRUE,
  );

  $entity_info['file']['view modes']['drupalimc_small_right'] = array(
    'label' => t('Small / Floating right'),
    'custom settings' => TRUE,
  );
}


/*
 * Implements hook_file_formatter_info
 */
function drupalimc_media_file_formatter_info() {
  // Create a new formatter.  This one is pretty basic, and probably will be 
  // replaced once the media formatters mature.  But for now, it's better for 
  // us than the default ones that media ships.

  $info = array();
  $info['drupalimc_image'] = array(
    'label' => t('Drupal IMC Image'),
    'default settings' => array(
      'image_style' => 'drupalimc_large',
      'link_to_original' => TRUE,
    ),
    'settings callback' => 'drupalimc_image_formatter_settings',
    'view callback' => 'drupalimc_image_formatter_view',
  );

  $info['drupalimc_file_icon'] = array(
    'label' => t('Drupal IMC File Icon'),
    'default settings' => array(
      'link_to_file' => TRUE,
      'width' => 180,
      'height' => 180,
    ),
    'settings callback' => 'drupalimc_file_icon_formatter_settings',
    'view callback' => 'drupalimc_file_icon_formatter_view',
  );

  $info['drupalimc_wysiwyg_placeholder'] = array(
    'label' => t('Drupal IMC Placeholder Image for WYSIWYG'),
    'default settings' => array(
      'width' => 180,
      'height' => 180,
    ),
    'settings callback' => 'drupalimc_wysiwyg_placeholder_formatter_settings',
    'view callback' => 'drupalimc_wysiwyg_placeholder_formatter_view',
  );

  return $info;
}

function drupalimc_image_formatter_settings($form, &$form_state, $settings) {

  $element = array();
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );

  $element['link_to_original'] = array(
    '#title' => t('Link to original image'),
    '#type' => 'checkbox',
    '#default_value' => $settings['link_to_original'],
  );

  return $element;
}

function drupalimc_image_formatter_view($file, $display, $langcode) {
  // Get the image element from file_entity to use as a base
  $image = file_entity_file_formatter_file_image_view($file, $display, $langcode);

  if($display['settings']['link_to_original']) {
    // NOTE: this doesn't work inside wysiwyg, but that's not that bad
    $width = $image['#width'] ? $image['#width'] : 500;
    $height = $image['#height'] ? $image['#height'] : 500;
    $colorbox_query = "?width=${width}&height=${height}";

    return array(
      '#type' => 'link',
      '#title' => render($image),
      '#options' => array(
        'html' => TRUE,
        'attributes' => array('class' => 'colorbox-load'),
      ),
      '#href' => file_create_url($file->uri) . $colorbox_query,
    );
  } else {
    return $image;
  }
}

function drupalimc_file_icon_formatter_settings($form, &$form_state, $settings) {

  $element = array();
  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
  );

  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
  );

  $element['link_to_file'] = array(
    '#title' => t('Link to file'),
    '#type' => 'checkbox',
    '#default_value' => $settings['link_to_file'],
  );

  return $element;
}

function drupalimc_file_icon_formatter_view($file, $display, $langcode) {
  $width = $display['settings']['width'];
  $height = $display['settings']['height'];
  $element = array();
  $element['icon'] = array(
    '#theme' => 'media_formatter_large_icon',
    '#file' => $file,
    '#attributes' => array(
      'width' => $width,
      'height' => $height,
    ),
  );

  $element['text'] = array(
    '#type' => 'markup',
    '#prefix' => '<br>',
    '#markup' => $file->filename,
  );

  if($display['settings']['link_to_file']) {
    return array(
      '#type' => 'link',
      '#title' => render($element),
      '#options' => array('html' => TRUE),
      '#href' => file_create_url($file->uri),
    );
  } else {
    return $element;
  }
}

function drupalimc_wysiwyg_placeholder_formatter_settings($form, &$form_state, $settings) {

  $element = array();
  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
  );

  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
  );

  return $element;
}

function drupalimc_wysiwyg_placeholder_formatter_view($file, $display, $langcode) {
  if (empty($file->override['wysiwyg'])) {
    // Only show this formatter in WYSIWYG mode
    return null;
  }

  $width = $display['settings']['width'];
  $height = $display['settings']['height'];
  return array(
    '#theme' => 'media_formatter_large_icon',
    '#file' => $file,
    '#attributes' => array(
      'width' => $width,
      'height' => $height,
    ),
  );
}

function drupalimc_media_form_media_format_form_alter(&$form, &$form_state) {
  // This form is displayed to pick a format when using the wysiwyg media
  // insert button.  Limit the options the user can choose from to the view 
  // modes that we set up.
  $allowed_formats = array(
    'drupalimc_large',
    'drupalimc_small_left',
    'drupalimc_small_right', 
  );
  $old_options = $form['options']['format']['#options'];
  $new_options = array();
  foreach($allowed_formats as $format) {
    $new_options[$format] = $old_options[$format];
  }
  $form['options']['format']['#options'] = $new_options;
}

/*
 * Implements hook_form_FORM_ID_alter
 */
function drupalimc_media_form_media_add_upload_alter(&$form, &$form_state) {
    $form['upload']['#size'] = 30;
}
 

function drupalimc_media_theme($existing, $type, $theme, $path) {
  return array(
    // located in drupalimc_media.fields.inc
    'drupalimc_media_existing_pluploads' => array(
      'render element' => 'element',
    ),
  );
}

