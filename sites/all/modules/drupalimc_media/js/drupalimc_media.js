(function ($) {
  // Store our function as a property of Drupal.behaviors.
  Drupal.behaviors.drupalimcMedia = {
    attach: function (context, settings) {
      $('a.drupalimc-media-gallery', context).click(function() {
        $('a.drupalimc-media-gallery').colorbox({
            'slideshow': true,
            'slideshowSpeed': 10000,
            'rel': 'drupalimc-media',
            'maxWidth': '90%',
            'maxHeight': '90%',
        });
      });
    }
  }
}(jQuery));

