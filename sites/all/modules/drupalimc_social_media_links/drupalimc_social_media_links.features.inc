<?php
/**
 * @file
 * drupalimc_social_media_links.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalimc_social_media_links_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
