<?php

/* Mostly taken from the colorbox login code.  Changed to redirect back to the
 * current page instead of user profile page
 */

function drupalimc_ui_login() {
  $form = drupal_get_form('user_login');
  // Redirect failed logins to the standard user login form.
  if (isset($_POST['form_id']) && $_POST['form_id'] == 'user_login') {
    return drupal_get_form('user_login');
  }

  print drupal_render($form);
  exit;
}
