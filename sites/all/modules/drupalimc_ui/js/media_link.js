(function ($) {
  // Store our function as a property of Drupal.behaviors.
  Drupal.behaviors.drupalimcUIInsertMediaLink = {
    attach: function (context, settings) {
      $('#drupalimc-article-node-form', context).each(function() {
        var text = Drupal.t('Insert Image/Audio/Video');
        var button = $('<a>' + text + '</a>')
                      .attr({href: '#'}).addClass('insert-media')
                      .click(function() {
          if($('#cke_edit-body-und-0-value').length > 0) {
          // CKEditor is active, use its button
            CKEDITOR.tools.callFunction(66, this); 
          } else {
          // Plaintext editor active, manually insert the tag
            selectMediaAndInsertTag(settings);
          }
          });
        $('.form-item-body-und-0-value', context)
          .find('label').once('insert-media').prepend(button);
      });
    }
  }

  function selectMediaAndInsertTag(settings) {
    Drupal.media.popups.mediaBrowser(function (mediaFiles) {
      var mediaFile = mediaFiles[0];
      var options = {};
      Drupal.media.popups.mediaStyleSelector(mediaFile,
        function (formattedMedia) {
          var body = $('#edit-body textarea.text-full');
          var mediaTagParts = [];
          mediaTagParts.push('"type":"media"');
          mediaTagParts.push('"view_mode":"' + formattedMedia.type + '"');
          mediaTagParts.push('"fid":"' + mediaFile.fid + '"');
          mediaTagParts.push('"attributes":{}');
          var mediaTag = '[[{ ' + mediaTagParts.join(", ") + '}]]\n';
          body.insertAtCaret(mediaTag);
        }, options);
    }, settings['global']);
  }
}(jQuery));

// Helper function found at:
// http://stackoverflow.com/questions/946534/insert-text-into-textarea-with-jquery
jQuery.fn.extend({
insertAtCaret: function(myValue){
  return this.each(function(i) {
    if (document.selection) {
      //For browsers like Internet Explorer
      this.focus();
      sel = document.selection.createRange();
      sel.text = myValue;
      this.focus();
    }
    else if (this.selectionStart || this.selectionStart == '0') {
      //For browsers like Firefox and Webkit based
      var startPos = this.selectionStart;
      var endPos = this.selectionEnd;
      var scrollTop = this.scrollTop;
      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
      this.focus();
      this.selectionStart = startPos + myValue.length;
      this.selectionEnd = startPos + myValue.length;
      this.scrollTop = scrollTop;
    } else {
      this.value += myValue;
      this.focus();
    }
  })
}
});

