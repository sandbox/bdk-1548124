<?php
/**
 * @file
 * drupalimc_users_and_permissions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drupalimc_users_and_permissions_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration pages.
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'system',
  );

  // Exported permission: access ckeditor link.
  $permissions['access ckeditor link'] = array(
    'name' => 'access ckeditor link',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'ckeditor_link',
  );

  // Exported permission: access content overview.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: access dashboard.
  $permissions['access dashboard'] = array(
    'name' => 'access dashboard',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'dashboard',
  );

  // Exported permission: access service links.
  $permissions['access service links'] = array(
    'name' => 'access service links',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'service_links',
  );

  // Exported permission: access toolbar.
  $permissions['access toolbar'] = array(
    'name' => 'access toolbar',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'toolbar',
  );

  // Exported permission: add media from remote sources.
  $permissions['add media from remote sources'] = array(
    'name' => 'add media from remote sources',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
      3 => 'editor',
    ),
    'module' => 'media_internet',
  );

  // Exported permission: administer comments.
  $permissions['administer comments'] = array(
    'name' => 'administer comments',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'comment',
  );

  // Exported permission: administer menu.
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'menu',
  );

  // Exported permission: administer nodes.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: administer users.
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'user',
  );

  // Exported permission: create drupalimc_article content.
  $permissions['create drupalimc_article content'] = array(
    'name' => 'create drupalimc_article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
      2 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: create drupalimc_document content.
  $permissions['create drupalimc_document content'] = array(
    'name' => 'create drupalimc_document content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: create drupalimc_event content.
  $permissions['create drupalimc_event content'] = array(
    'name' => 'create drupalimc_event content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
      2 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: create drupalimc_feature content.
  $permissions['create drupalimc_feature content'] = array(
    'name' => 'create drupalimc_feature content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: create drupalimc_migrated_feature content.
  $permissions['create drupalimc_migrated_feature content'] = array(
    'name' => 'create drupalimc_migrated_feature content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create url aliases.
  $permissions['create url aliases'] = array(
    'name' => 'create url aliases',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'path',
  );

  // Exported permission: crop any image.
  $permissions['crop any image'] = array(
    'name' => 'crop any image',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'imagecrop',
  );

  // Exported permission: delete any drupalimc_article content.
  $permissions['delete any drupalimc_article content'] = array(
    'name' => 'delete any drupalimc_article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any drupalimc_document content.
  $permissions['delete any drupalimc_document content'] = array(
    'name' => 'delete any drupalimc_document content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any drupalimc_event content.
  $permissions['delete any drupalimc_event content'] = array(
    'name' => 'delete any drupalimc_event content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any drupalimc_feature content.
  $permissions['delete any drupalimc_feature content'] = array(
    'name' => 'delete any drupalimc_feature content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any drupalimc_migrated_feature content.
  $permissions['delete any drupalimc_migrated_feature content'] = array(
    'name' => 'delete any drupalimc_migrated_feature content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own drupalimc_article content.
  $permissions['delete own drupalimc_article content'] = array(
    'name' => 'delete own drupalimc_article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own drupalimc_document content.
  $permissions['delete own drupalimc_document content'] = array(
    'name' => 'delete own drupalimc_document content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own drupalimc_event content.
  $permissions['delete own drupalimc_event content'] = array(
    'name' => 'delete own drupalimc_event content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own drupalimc_feature content.
  $permissions['delete own drupalimc_feature content'] = array(
    'name' => 'delete own drupalimc_feature content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own drupalimc_migrated_feature content.
  $permissions['delete own drupalimc_migrated_feature content'] = array(
    'name' => 'delete own drupalimc_migrated_feature content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete terms in 1.
  $permissions['delete terms in 1'] = array(
    'name' => 'delete terms in 1',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit any drupalimc_article content.
  $permissions['edit any drupalimc_article content'] = array(
    'name' => 'edit any drupalimc_article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any drupalimc_document content.
  $permissions['edit any drupalimc_document content'] = array(
    'name' => 'edit any drupalimc_document content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any drupalimc_event content.
  $permissions['edit any drupalimc_event content'] = array(
    'name' => 'edit any drupalimc_event content',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any drupalimc_feature content.
  $permissions['edit any drupalimc_feature content'] = array(
    'name' => 'edit any drupalimc_feature content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any drupalimc_migrated_feature content.
  $permissions['edit any drupalimc_migrated_feature content'] = array(
    'name' => 'edit any drupalimc_migrated_feature content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own drupalimc_article content.
  $permissions['edit own drupalimc_article content'] = array(
    'name' => 'edit own drupalimc_article content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
      2 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own drupalimc_document content.
  $permissions['edit own drupalimc_document content'] = array(
    'name' => 'edit own drupalimc_document content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own drupalimc_event content.
  $permissions['edit own drupalimc_event content'] = array(
    'name' => 'edit own drupalimc_event content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
      2 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own drupalimc_feature content.
  $permissions['edit own drupalimc_feature content'] = array(
    'name' => 'edit own drupalimc_feature content',
    'roles' => array(
      0 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own drupalimc_migrated_feature content.
  $permissions['edit own drupalimc_migrated_feature content'] = array(
    'name' => 'edit own drupalimc_migrated_feature content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit terms in 1.
  $permissions['edit terms in 1'] = array(
    'name' => 'edit terms in 1',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: revert revisions.
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: update any fullcalendar event.
  $permissions['update any fullcalendar event'] = array(
    'name' => 'update any fullcalendar event',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'fullcalendar',
  );

  // Exported permission: use text format filtered_html.
  $permissions['use text format filtered_html'] = array(
    'name' => 'use text format filtered_html',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format full_html.
  $permissions['use text format full_html'] = array(
    'name' => 'use text format full_html',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'filter',
  );

  // Exported permission: view own unpublished content.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: view the administration theme.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      0 => 'administrator',
      1 => 'editor',
    ),
    'module' => 'system',
  );

  return $permissions;
}
