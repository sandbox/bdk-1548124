<?php
/**
 * @file
 * flag_hidden.features.inc
 */

/**
 * Implements hook_views_api().
 */
function flag_hidden_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function flag_hidden_flag_default_flags() {
  $flags = array();
  // Exported flag: "Hidden Comment".
  $flags['hidden_comment'] = array(
    'content_type' => 'comment',
    'title' => 'Hidden Comment',
    'global' => '1',
    'types' => array(),
    'flag_short' => 'Hide Comment',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unhide Comment',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'normal',
    'roles' => array(
      'flag' => array(),
      'unflag' => array(),
    ),
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'flag_hidden',
    'api_version' => 2,
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Hidden Node".
  $flags['hidden_node'] = array(
    'content_type' => 'node',
    'title' => 'Hidden Node',
    'global' => '1',
    'types' => array(),
    'flag_short' => 'Hide [node:type-name]',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unhide [node:type-name]',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'normal',
    'roles' => array(
      'flag' => array(),
      'unflag' => array(),
    ),
    'show_on_page' => 0,
    'show_on_teaser' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'flag_hidden',
    'api_version' => 2,
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;
}
