(function ($) {
  var toggle_link = null;

  Drupal.behaviors.flagHidden = {
    attach: function (context, settings) {
      var hidden_comments = $('.hidden-comment', context);

      if(hidden_comments.length > 0) {
        build_toggle_link();
        add_header_note(hidden_comments.length);
        add_rehide_links();
        $('.hidden-comment', context).hide();
      }
    }
  }

  function build_toggle_link() {
    // Make a link to toggle showing hidden comments and put it in the
    // toggle_link variable
    toggle_link = $('<a href="#">');
    toggle_link.append(Drupal.t('Show hidden comments'));
    toggle_link.mode = 'show';
    toggle_link.click(function() {
      if(toggle_link.mode == 'show') {
        show_hidden_comments();
      } else {
        hide_hidden_comments();
      }
      return false;
    });
  }

  function add_header_note(hidden_count) {
    // Add a note right below the "Comments" header explaining that some
    // comments are hidden
    var note = $('<div class="hidden-comments-heading clearfix"></div>');
    note.append(Drupal.formatPlural(hidden_count,
      'One comment hidden by the editors',
      '@count comments hidden by the editors',
      {}
    ));
    note.append(toggle_link);

    $('.comment').filter(':first').before(note);
  }

  function add_rehide_links() {
    // Add links near the titles for hidden comments to re-hide thew once
    // they've been shown.
    var link = $('<a href="#" class="hide-comments-link"></a>');
    link.append(Drupal.t("Don't show hidden comments"));
    link.click(function() {
        hide_hidden_comments();
        return false;
    });
    $('.hidden-comment').prepend(link);
  }

  function show_hidden_comments() {
    // Show all hidden comments using an animation
    $('.hidden-comment').fadeIn(800);
    var top = $(".hidden-comment").filter(':first').offset().top;
    $('html, body').animate({
      scrollTop: top - 30,
      }, 200);
    toggle_link.html(Drupal.t("Don't show hidden comments"));
    toggle_link.mode = 'hide';
  }

  function hide_hidden_comments() {
    // Hide all hidden comments using an animation
    $('.hidden-comment').fadeOut(400);
    toggle_link.html(Drupal.t('Show hidden comments'));
    toggle_link.mode = 'show';
  }
}(jQuery));
