<?php

/*
 * Create terms in a vocabulary as we need them.
 */
class MigrateDadaTermCreator {
  public function __construct($machine_name) {
    $vid_query = db_select('taxonomy_vocabulary', 'tv')
      ->fields('tv', array('vid'))
      ->condition('machine_name', $machine_name);
    $this->vid = $vid_query->execute()->fetchField();

    // term_map will map taxonomy term names to their tids.
    $this->term_map = array();
    $term_query = db_select('taxonomy_term_data', 'ttd')
      ->fields('ttd', array('tid', 'name'))
      ->condition('ttd.vid', $this->vid);
    foreach($term_query->execute() as $row) {
      $this->term_map[$row->name] = $row->tid;
    }
  }

  // Create a term if it doesn't already exist
  public function ensureTermExists($term_name) {
    if(!array_key_exists($term_name, $this->term_map)) {
      $term = (object)array(
        'vid' => $this->vid,
        'name' => $term_name,
      );
      taxonomy_term_save($term);
      $this->term_map[$term_name] = $term->tid;
    }
  }

  public function ensureTermsExist($term_names) {
    foreach($term_names as $term_name) {
      $this->ensureTermExists($term_name);
    }
  }
}

abstract class DadaArticleMigrationBase extends Migration {
  public function __construct($dada_type) {
    parent::__construct(MigrateGroup::getInstance('dadaimc'));

    $node_type = variable_get("migrate_dada_${dada_type}_node_type");
    $local_field = variable_get("migrate_dada_${dada_type}_local_field");
    $category_field = variable_get("migrate_dada_${dada_type}_category_field");
    $section_field = variable_get("migrate_dada_${dada_type}_section_field");
    $media_field = variable_get("migrate_dada_${dada_type}_media_field");

    if(!$node_type) {
      drush_die("Need to run drush migrate-setup-dada-$dada_type");
    }

    $this->user_map = MigrationBase::getInstance('DadaUser')->map;

    $this->source = new MigrateSourceSQL($this->buildQuery());
    $options = MigrateDestinationNode::options(LANGUAGE_NONE, 'full_html');
    $this->destination = new MigrateDestinationNode($node_type, $options);
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'objectid' => array('type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema(),
      'migrate_dada'
    );
    /* For ui get the author from our dada user migration and fall back to 
     * anonymous.
     */
    $this->addFieldMapping('uid', 'authorid')
      ->sourceMigration("DadaUser")
      ->defaultValue(0);
    $this->addFieldMapping('title', 'heading');
    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('promote', 'feature');
    $this->addFieldMapping('changed', 'modified_timestamp');
    $this->addFieldMapping('status', null)->defaultValue(1);
    if($local_field) {
      $this->addFieldMapping($local_field, 'local_interest');
    }
    $this->category_creator = $this->section_creator = null;
    if($category_field) {
      $this->addFieldMapping($category_field, 'categories');
      $category_vname = $this->getVocabularyFromField($category_field);
      if($category_vname) {
        $this->category_creator = new MigrateDadaTermCreator($category_vname);
      }
    }
    if($section_field) {
      $this->addFieldMapping($section_field, 'section');
      $section_vname =  $this->getVocabularyFromField($section_field);
      if($section_vname) {
        $this->section_creator = new MigrateDadaTermCreator($section_vname);
      }
    }
    if($media_field) {
      $args = array(
        'file_function' => 'file_copy',
        'file_replace' => FILE_EXISTS_REPLACEi,
      );
      $this->addFieldMapping($media_field, 'images')->arguments($args);
    }
  }

  private function getVocabularyFromField($field_name) {
    $field = field_read_field($field_name);
    return $field['settings']['allowed_values'][0]['vocabulary'];
  }

  public function prepareRow($row) {
    $row->categories = $this->getCategories($row->objectid);
    $row->image_infos = $this->getImages($row->objectid);
    $row->images = array();
    foreach($row->image_infos as $info) {
      $row->images[] = $this->filePath($info['filename'], $info['mime_class']);
    }
    $row->body = $this->formatBody($row);
    if($this->category_creator) {
      $this->category_creator->ensureTermsExist($row->categories);
    }
    if($this->section_creator) {
      $this->section_creator->ensureTermExists($row->section);
    }
  }

  public function getImages($dada_objectid) {
    $images = array();
    $query = Database::getConnection('default', 'migrate_dada')
      ->select('media', 'm')
      ->fields('m', array('filename', 'mime_class', 'alignment'))
      ->condition('deleted', 0)
      ->condition('parentid', $dada_objectid);
    foreach($query->execute() as $row) {
      $images[] = array(
        'filename' => $row->filename,
        'mime_class' => $row->mime_class,
        'alignment' => $row->alignment,
      );
    }
    return $images;
  }

  public function getCategories($dada_objectid) {
    $query = Database::getConnection('default', 'migrate_dada')
      ->select('categories', 'c')
      ->fields('c', array('catname'))
      ->condition('c.deleted', 0);
    $query->join('hash_category', 'hc', 'hc.category_id = c.objectid');
    $query->condition('hc.ref_class', 'Article');
    $query->condition('hc.ref_id', $dada_objectid);
    return $query->execute()->fetchCol();
  }

  public function filePath($filename, $mime_class) {
    return implode('/', array(dirname(__FILE__), 'usermedia', $mime_class,
      $this->fileSubdir($filename), $filename));
  }

  /**
   * Copied from dadaimc. Used to sortof hash media files into directories
   * 1..13.
   */
  public function fileSubdir($filename) {
    // gets a number between 1 and 13 using the first character
    $num = 0;
    for ($x = 0; $x < strlen($filename); $x++) {
      $num += ord($filename[$x]);
    }
    return ($num % 13) + 1;
  }

  public function formatBody(&$row) {
    if($row->summary) {
      // Article with a summary and body
      $body_top = $row->summary . "\n<!--break-->\n";
      return $body_top . $this->bodyWithMedia($row->body, $row->image_infos, $row->embedded_media);
    } else {
      // Article with just a body
      return $this->bodyWithMedia($row->body, $row->image_infos, $row->embedded_media);
    }
  }

  function bodyWithMedia($body, $image_infos, $embedded_media) {
    if (count($image_infos) == 0) {
      return $body;
    }

    $all_images = array();
    foreach($image_infos as $image_info) {
      $all_images[] = $this->imageTag($image_info);
    }

    $top = '';
    $bottom = '';

    switch ($embedded_media) {
      case 'oneaftersummary':
        $top = array_shift($all_images);
        break;

      case 'placeholders':
        $i = 1;
        while($image = array_shift($all_images)) {
          $body = str_replace("#file_$i#", $image, $body);
          $i++;
        }
        break;

      case 'aftersummary':
        $top = implode("\n", $all_images);
        $all_images = array();
        break;
    }

    $bottom = implode("\n", $all_images);

    return $top . $body . $bottom;
  }

  function imageTag($image_info) {
    $uri = file_build_uri("migrate_dada/" . $image_info['filename']);
    $src = file_create_url($uri);
    if($image_info['alignment'])  {
      return '<img src="' . $src . '" align="' . $image_info['alignment'] . '">';
    } else {
      return '<img class="dada-image-center" src="' . $src . '"><br>';
    }
  }
}

class DadaArticleMigration extends DadaArticleMigrationBase {
  public function __construct() {
    parent::__construct('article');
  }

  public function buildQuery() {
    $query = Database::getConnection('default', 'migrate_dada')
      ->select('articles', 'a')
      ->fields('a', array('objectid', 'modified_timestamp', 'section', 'language', 'authorid', 'heading', 'summary', 'body', 'feature', 'local_interest',
        'embedded_media'))
      ->condition('deleted', 0)
      ->condition('displayable', 1)
      ->condition('submitted', 1);
    $query->addExpression('UNIX_TIMESTAMP(created_datetime)', 'created');
    return $query;
  }
}

class DadaFeatureMigration extends DadaArticleMigrationBase {
  public function __construct() {
    parent::__construct('feature');
    $this->article_map = MigrationBase::getInstance('DadaArticle')->map;
  }

  public function buildQuery() {
    $query = Database::getConnection('default', 'migrate_dada')
      ->select('features', 'f')
      ->fields('f', array('objectid', 'modified_timestamp', 'section', 'language', 'authorid', 'heading', 'embedded_media', 'ref_class', 'refid'))
      ->condition('displayable', 1)
      ->condition('submitted', 1);
    $query->addExpression('UNIX_TIMESTAMP(created_datetime)', 'created');
    // Use expressions for some columns that are always true for feature
    $query->addExpression('1', 'feature');
    $query->addExpression('1', 'local_interest');
    // Use the summary as the body
    $query->addExpression('NULL', 'summary');
    $query->addField('f', 'summary', 'body');
    return $query;
  }

  public function formatBody(&$row) {
    $body = parent::formatBody($row);
    if($row->ref_class == 'Article') {
      $dest_id = $this->article_map->lookupDestinationID(array($row->refid));
      $nid = $dest_id['destid1'];
      $body .= "\n<p>" . l(_('Read Full Article'), "node/$nid") . "</p>";
    }
    return $body;
  }
}
