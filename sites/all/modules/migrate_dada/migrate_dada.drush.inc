<?php

function migrate_dada_drush_command() {
  $items = array(
    'migrate-setup-dada-article' => array(
      'description' => t("Setup migration for Dada Articles"),
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
    ),
    'migrate-setup-dada-feature' => array(
      'description' => t("Setup migration for Dada Features"),
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
    ),
  );

  return $items;
}

function drush_migrate_dada_migrate_setup_dada_article() {
  drush_migrate_dada_migrate_setup_dada_type('article');
}

function drush_migrate_dada_migrate_setup_dada_feature() {
  drush_migrate_dada_migrate_setup_dada_type('feature');
}

function drush_migrate_dada_migrate_setup_dada_type($dada_type) {
  $vars = array();

  $type_choices = array();
  foreach(node_type_get_types() as $machine_name => $type) {
    $type_choices[$machine_name] = $type->name;
  }
  $node_type = drush_choice($type_choices,
    "Choose node type to migrate dada $dada_type data to");
  if($node_type === FALSE) return;
  $vars['node_type'] = $node_type;

  $fields = array();
  foreach(field_info_instances('node', $node_type) as $name => $field) {
    $fields[$name] = $field['label'];
  }
  $fields[null] = 'None';

  if($dada_type != 'feature') {
    $choice = drush_choice($fields,
      t("Choose field to store local interest to"));
    if($choice === FALSE) return;
    $vars['local_field'] = $choice;
    if($choice) unset($fields[$choice]);
  }

  $choice = drush_choice($fields,
    t("Choose field to store category terms to"));
  if($choice === FALSE) return;
  $vars['category_field'] = $choice;
  if($choice) unset($fields[$choice]);

  $choice = drush_choice($fields,
    t("Choose field to store section terms to"));
  if($choice === FALSE) return;
  $vars['section_field'] = $choice;
  if($choice) unset($fields[$choice]);

  $choice = drush_choice($fields,
    t("Choose field to store media to"));
  if($choice === FALSE) return;
  $vars['media_field'] = $choice;
  if($choice) unset($fields[$choice]);

  drush_print("");
  drush_print("Setup complete.  Setting variables:");
  $vars_to_set = array();
  foreach($vars as $var_name => $value) {
    $real_var_name = "migrate_dada_" . $dada_type . '_' . $var_name;
    $vars_to_set[$real_var_name] = $value;
    drush_print($real_var_name . ' => ' . $value);
  }
  drush_print("");

  foreach($vars_to_set as $var => $value) {
    variable_set($var, $value);
  }
  drush_print("Next step: drush mi Dada" . ucfirst($dada_type));
}
