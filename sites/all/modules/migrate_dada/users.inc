<?php

class DadaUserMigration extends Migration {

  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('dadaimc'));

    $query = Database::getConnection('default', 'migrate_dada')
      ->select('users', 'u')
      ->fields('u', array('objectid', 'modified_timestamp', 'modified_by', 
        'username', 'password', 'old_lastaccess_timestamp', 
        'lastaccess_timestamp', 'chash', 'fullname', 'firstname', 'lastname', 
        'level', 'phone', 'address', 'address2', 'city', 'state', 'zip', 
        'country', 'email', 'email_verified', 'set_cookie', 'language', 
        'stylesheet', 'default_section', 'default_category', 'is_public', 
        'pw_method', 'alt_email', 'homepage', 'chat_client', 'chat_id', 
        'organizations', 'biography', 'photo'))
      ->condition('deleted', 0, '=');

    $query->addExpression('UNIX_TIMESTAMP(created_datetime)', 'created');

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser(array(
      'md5_passwords' => TRUE,
    ));
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'objectid' => array('type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationUser::getKeySchema(),
      'migrate_dada'
    );
    $this->addFieldMapping('name', 'username');
    $this->addFieldMapping('pass', 'password');
    $this->addFieldMapping('mail', 'email');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('status', null)->defaultValue(1);
  }
}
