<?php
/*
 * Page layout code.
 *
 * We try to use HTML 5 semantic elements when possible.  normalize.css should 
 * take care of making them work like a div for browsers that don't support 
 * them.  We also try to specify ARIA roles.
 */

?>

<div id="page" class="<?php echo $classes;?>">
    <?php if ($main_menu || $page['navbar']) : ?>
      <div id="navbar-wrapper">
      <div id="navbar" class="clearfix">
      <?php print render($page['navbar']); ?>
      <nav id="navigation" role="navigation">
        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'clearfix')))) ?>
        <?php print theme('links__system_main_menu', array('links' => array_reverse($secondary_menu), 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'clearfix')))) ?>
      </nav> <!-- /#navigation -->
      </div></div>
    <?php endif; ?>

   <div id="header-wrapper">
   <header id="header" class="clearfix" role="banner">
     <?php if ($banner_image): ?>
       <a id="banner-image" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $banner_image; ?>" /></a>
     <?php endif; ?>

     <?php if ($site_name || $site_slogan): ?>
       <div id="name-and-slogan">
         <?php if ($site_name): ?>
             <h1 id="site-name">
               <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
             </h1>
         <?php endif; ?>
 
         <?php if ($site_slogan): ?>
           <p id="site-slogan"><?php print $site_slogan; ?></p>
         <?php endif; ?>
       </div> <!-- /#name-and-slogan -->
     <?php endif; ?>

  </header></div> <!-- /#header, /#header-wrapper -->


  <div id="main-wrapper"><div id="main" class="clearfix">
    <?php print $messages; ?>

    <div id="main-after-messages">

    <?php if ($page['highlighted']): ?>
      <div id="highlighted" class="sidebar">
             <?php print render($page['highlighted']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['featured']): ?>
      <div id="featured" class="main-column">
    <?php print render($page['featured']); ?>
      </div>
    <?php endif; ?>
 

    <?php if ($page['highlighted'] || $page['sidebar_first'] || $page['sidebar_second']): ?>
    <div class="main-column">
    <div id="content" class="sidebars" role="main">
    <?php else: ?>
    <div id="content" class="no-sidebars" role="main">
    <?php endif; ?>
      <?php if ($breadcrumb): ?>
        <div id="breadcrumb"><?php print $breadcrumb; ?></div>
      <?php endif; ?>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if (isset($hidden_node) and $hidden_node): ?>
        <div class="hidden-status"><?php print t('hidden') ?></div>
      <?php endif; ?>
      <?php if ($local_node): ?>
        <div class="local-status"><?php print t('local interest') ?></div>
      <?php endif; ?>
      <?php if ($tabs): ?>
        <div id="page-tabs"><?php print render($tabs); ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div> <!-- /#content -->
    </div>

    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="sidebar" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="sidebar" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </div>
    <?php endif; ?>

  </div></div></div> <!-- /#main-after-messages /#main, /#main-wrapper -->

  <footer id="footer" role="contentinfo">
    <?php print render($page['footer']); ?>
  </footer>

</div> <!-- /#page -->
