<?php

/**
 * code that should be executed for every page load
 */

function delivery_init() {
  $banner_pad_top = theme_get_setting('banner_pad_top');
  $banner_pad_bottom = theme_get_setting('banner_pad_bottom');
  if($banner_pad_top || $banner_pad_bottom) {
    $css = '#banner-image { ';
    if($banner_pad_top) {
      $css .= "margin-top: ${banner_pad_top}px; ";
    }
    if($banner_pad_bottom) {
      $css .= "margin-bottom: ${banner_pad_bottom}px ";
    }
    $css .= '}';
    drupal_add_css($css, array('type' => 'inline',
                               'group' => CSS_THEME,
                               'every_page' => TRUE));
  }
}

/* Theme the search block form */
function delivery_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#attributes']['placeholder'] = t('search...');
  }
}

function delivery_preprocess_page(&$vars) {
  $banner_path = theme_get_setting('banner_path');
  if($banner_path) {
    $vars['banner_image'] = file_create_url($banner_path);
  } else {
    $vars['banner_image'] = '';
  }
  if(isset($vars['node']->field_local_interest) &&
     $vars['node']->field_local_interest['und'][0]['value']) {
    $vars['local_node'] = TRUE;
  } else {
    $vars['local_node'] = FALSE;
  }
  if(module_exists("flag_hidden") && flag_hidden_page_is_hidden_node()) {
    $vars['classes_array'][] = 'hidden-node';
  }
}

function delivery_preprocess_link(&$vars) {
  if($vars['path'] === 'user/login' && !isset($vars['options']['query'])) {
    $dest = drupal_get_destination();
    if($dest['destination'] == variable_get('site_frontpage')) {
      $dest['destination'] = '<front>';
    }
    $vars['options']['query'] = $dest;
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function delivery_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('breadcrumb_display');
  if ($show_breadcrumb == 'yes') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $separator = filter_xss(theme_get_setting('breadcrumb_separator'));
      $trailing_separator = $title = '';

      // Add the title and trailing separator
      if (theme_get_setting('breadcrumb_title')) {
        if ($title = drupal_get_title()) {
          $trailing_separator = $separator;
        }
      }
      // Just add the trailing separator
      elseif (theme_get_setting('breadcrumb_trailing')) {
        $trailing_separator = $separator;
      }

      // Assemble the breadcrumb
      return implode($separator, $breadcrumb) . $trailing_separator . $title;
    }
  }
  // Otherwise, return an empty string.
  return '';
}

function delivery_image($variables) {
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);

  // Don't include the width/height tags.  This makes it hard to resize them 
  // with the image.
  foreach (array('alt', 'title') as $key) {

    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }

  return '<img' . drupal_attributes($attributes) . ' />';
}

function delivery_comment_block() {
  $rows = array();
  $number = variable_get('comment_block_count', 10);
  foreach (comment_get_recent($number) as $comment) {
    $link = l($comment->subject, 'comment/' . $comment->cid,
      array('fragment' => 'comment-' . $comment->cid));
    $time = t('@time ago',
      array('@time' => format_interval(REQUEST_TIME - $comment->changed)));
    $rows[] = array($link, array('data' => $time, 'class' => 'time'));
  }

  if ($rows) {
    return theme('table', array('rows' => $rows));
  }
  else {
    return t('No comments available.');
  }
}

function delivery_preprocess_node(&$vars) {
  /* We handle the local interest field in preprocess_html */
  if(isset($vars['content']['field_local_interest'])) {
    unset($vars['content']['field_local_interest']);
  }
}

function delivery_preprocess_field(&$vars, $hook) {
  // Add line breaks to plain text textareas.
  if ($vars['element']['#field_type'] == 'text_long' &&
    $vars['element']['#items'][0]['format'] == null) {
      $vars['items'][0]['#markup'] = nl2br($vars['items'][0]['#markup']);
    }
}


delivery_init();
