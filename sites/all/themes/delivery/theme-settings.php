<?php

function delivery_form_system_theme_settings_alter(&$form, &$form_state) {
  delivery_disable_logo_settings($form, $form_state);
  delivery_add_breadcrumb_settings($form, $form_state);
  delivery_add_banner_upload($form, $form_state);

  $form['#validate'][] = 'delivery_settings_validate';
  $form['#submit'][] = 'delivery_settings_submit';
}

function delivery_disable_logo_settings(&$form, &$form_state) {
  $form['logo']['#access'] = FALSE;
  $form['theme_settings']['toggle_logo']['#access'] = FALSE;
}

function delivery_add_breadcrumb_settings(&$form, &$form_state) {
  /**
   * Breadcrumb settings
   * Copied from Zen
   */
  $form['breadcrumb'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breadcrumb'),
  );
  $form['breadcrumb']['breadcrumb_display'] = array(
    '#type' => 'select',
    '#title' => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_display'),
    '#options' => array(
      'yes' => t('Yes'),
      'no' => t('No'),
    ),
  );
  $form['breadcrumb']['breadcrumb_separator'] = array(
    '#type'  => 'textfield',
    '#title' => t('Breadcrumb separator'),
    '#description' => t('Text only. Dont forget to include spaces.'),
    '#default_value' => theme_get_setting('breadcrumb_separator'),
    '#size' => 8,
    '#maxlength' => 10,
  );
  $form['breadcrumb']['breadcrumb_home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the homepage link in breadcrumbs'),
    '#default_value' => theme_get_setting('breadcrumb_home'),
  );
  $form['breadcrumb']['breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_trailing'),
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
  );
  $form['breadcrumb']['breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_title'),
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
  );
}

function delivery_add_banner_upload(&$form, &$form_state) {
    $form['banner_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Upload Banner Image'),
    );
    $banner_path = theme_get_setting('banner_path');
    $banner_pad_top = theme_get_setting('banner_pad_top');
    $banner_pad_bottom = theme_get_setting('banner_pad_bottom');
    // If $banner_path is a public:// URI, display the path relative to the files
    // directory; stream wrappers are not end-user friendly.
    if (file_uri_scheme($banner_path) == 'public') {
      $banner_path = file_uri_target($banner_path);
    }

    $form['banner_settings']['banner_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to custom banner'),
      '#default_value' => $banner_path,
      '#description' => t('The path to the file you would like to use as your banner.'),
    );
    $form['banner_settings']['banner_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload banner image'),
      '#maxlength' => 40,
      '#description' => t("If you don't have direct file access to the server, use this field to upload your banner.")
    );

    $form['banner_settings']['layout'] = array(
      '#type' => 'fieldset',
      '#title' => t('Layout Banner'),
    );

    $form['banner_settings']['layout']['banner_pad_top'] = array(
      '#type' => 'textfield',
      '#title' => t('Banner Top padding (px)'),
      '#default_value' => $banner_pad_top,
    );
    $form['banner_settings']['layout']['banner_pad_bottom'] = array(
      '#type' => 'textfield',
      '#title' => t('Banner Bottom Padding (px)'),
      '#default_value' => $banner_pad_bottom,
    );
    
}

function delivery_settings_validate($form, &$form_state) {
  // Handle file uploads.
  $validators = array('file_validate_is_image' => array());

  // Check for a new uploaded logo.
  $file = file_save_upload('banner_upload', $validators);
  if (isset($file)) {
    // File upload was attempted.
    if ($file) {
      // Put the temporary file in form_values so we can save it on submit.
      $form_state['values']['banner_upload'] = $file;
    }
    else {
      // File upload failed.
      form_set_error('banner_upload', t('The banner could not be uploaded.'));
    }
  }

  // If the user provided a path for a logo or favicon file, make sure a file
  // exists at that path.
  if ($form_state['values']['banner_path']) {
    $path = _system_theme_settings_validate_path($form_state['values']['banner_path']);
    if (!$path) {
      form_set_error('banner_path', t('The custom banner path is invalid.'));
    }
  }

  // Check that banner padding is numeric
  foreach(array('banner_pad_top', 'banner_pad_bottom') as $key) {
    if ($form_state['values'][$key] &&
        !is_numeric($form_state['values'][$key])) {
      form_set_error($key, t('Padding must be a number.'));
    }
  }
}

function delivery_settings_submit($form, &$form_state) {
  $values = &$form_state['values'];
  // If the user uploaded a new banner image, save it to a permanent location
  if ($file = $values['banner_upload']) {
    unset($values['banner_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['banner_path'] = $filename;
  }

  // If the user entered a path relative to the system files directory for
  // a banner image, store a public:// URI so the theme system can handle it.
  if (!empty($values['banner_path'])) {
    $values['banner_path'] = _system_theme_settings_validate_path($values['banner_path']);
  }
}
